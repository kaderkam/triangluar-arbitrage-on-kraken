# Trojúhelníková arbitráž na kryptosměnárně Kraken

Vyhledávání živých trojúhelníkových arbitráží na kryptosměnárně Kraken.

## Instalační příručka

Aplikace je napsaná v jayzce Python ve verzi 3.

### Balíčky potřebné pro spuštění v Pythonu:
- requests
- websocket-client
- Django
- djangorestframework
